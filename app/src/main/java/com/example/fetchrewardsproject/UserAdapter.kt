package com.example.fetchrewardsproject

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.fetchrewardsproject.model.UserModel

class CustomAdapter(context: Context, private val arrayListDetails: ArrayList<UserModel>) : BaseAdapter(){
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return arrayListDetails.size
    }

    override fun getItem(position: Int): Any {
        return arrayListDetails[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val listRowHolder: ListRowHolder
        if (convertView == null) {
            view = this.layoutInflater.inflate(R.layout.user_layout, parent, false)
            listRowHolder = ListRowHolder(view)
            view.tag = listRowHolder
        } else {
            view = convertView
            listRowHolder = view.tag as ListRowHolder
        }

        listRowHolder.tvListId.text = String.format("List ID: %s", arrayListDetails[position].listId.toString())
        listRowHolder.tvName.text = String.format("Name: %s", arrayListDetails[position].name)
        listRowHolder.tvId.text = String.format("ID: %s", arrayListDetails[position].id.toString())
        return view
    }
}

private class ListRowHolder(row: View?) {
    val tvId: TextView = row?.findViewById(R.id.tvId) as TextView
    val tvListId: TextView = row?.findViewById(R.id.tvListId) as TextView
    val tvName: TextView = row?.findViewById(R.id.tvName) as TextView
}