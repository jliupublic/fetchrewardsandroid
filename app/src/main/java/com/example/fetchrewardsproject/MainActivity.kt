package com.example.fetchrewardsproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ListView
import android.widget.ProgressBar
import com.example.fetchrewardsproject.model.UserModel
import java.io.IOException
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import okhttp3.*
import org.json.JSONArray


class MainActivity : AppCompatActivity() {
    lateinit var progress: ProgressBar
    lateinit var listViewDetails: ListView
    var arrayListDetails: ArrayList<UserModel> = ArrayList()
    private val client = OkHttpClient()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progress = findViewById(R.id.progressBar)
        progress.visibility = View.VISIBLE
        listViewDetails = findViewById(R.id.listView)
        run()
    }

    private fun run() {
        val url = "https://fetch-hiring.s3.amazonaws.com/hiring.json"
        // progress.visibility = View.VISIBLE
        val request = Request.Builder()
            .url(url)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                progress.visibility = View.GONE
            }

            override fun onResponse(call: Call, response: Response) {
                val strResponse = response.body()!!.string()
                val jsonArray = JSONArray(strResponse)

                val userMap = HashMap<Int, ArrayList<Pair<Int, String>>>()

                for (i in 0 until jsonArray.length()) {
                    val user = jsonArray.getJSONObject(i)
                    val name = user.optString("name")
                    // filter blank or null names
                    if (name.isNullOrEmpty() || name.equals("null")) {
                        continue
                    }
                    val id = user.optString("id").toInt()
                    val listId = user.optString("listId").toInt()

                    // group by listId
                    val idPair = userMap[listId]
                    if (idPair == null) {
                        val newPair = arrayListOf(Pair(id, name))
                        userMap[listId] = newPair
                    }
                    else {
                        idPair.add(Pair(id, name))
                    }
                }

                val sorted = userMap.toSortedMap(compareBy { it } ) // sort by listId
                for ((k, v) in sorted) {
                    // then sort by name
                    v.sortBy { Integer.parseInt(it.second.replace("[\\D]".toRegex(), "")) }
                    for ((id, name) in v) {
                        val model = UserModel()
                        model.listId = k
                        model.id = id
                        model.name = name
                        arrayListDetails.add(model)
                    }
                }
                runOnUiThread {
                    val objAdapter =
                        CustomAdapter(applicationContext, arrayListDetails)
                    listViewDetails.adapter = objAdapter

                    progress.visibility = View.GONE
                }
            }
        })
    }
}