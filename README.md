# Fetch Rewards Android Project
An Android app that displays a list of grouped and sorted items to the user.

# Getting started
1. Clone this repository
2. Install Android Studio
3. Open this repository in Android Studio
4. Trust this project (or preview if you would like to check)  
  To be built, this project needs to be trusted.
5. Create a new configuration with module: `Fetch_Rewards_Project.app`
6. Run the configuration! Make sure you have configured a virtual device if running in Android Studio
7. Build APK if desired.

# Usage
Scroll up and down to see all items in the list.